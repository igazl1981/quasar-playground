import axios from 'axios'

let todos = []

function saveTodos () {
  localStorage.setItem('todos', JSON.stringify(todos))
}

async function refresh () {
  const response = await axios.get('/api/v4/todos')
  todos = response.data
  return todos
}

async function add (todo) {
  const newTodo = {
    done: false,
    text: todo
  }
  const response = await axios.post('/api/v4/todos', newTodo)
  todos.push(response.data)
  return todos
}

function get () {
  return todos
}

async function remove (id) {
  console.log('Delete v4 todo: ' + id)
  const todo = (await axios.delete('/api/v4/todos/' + id)).data
  console.log('Deleted todo: ' + JSON.stringify(todo))
  const index = findIndexById(todo.id)
  console.log('Delete todo index: ' + index)
  if (index !== undefined && index > -1) {
    todos.splice(index, 1)
  }
  console.log('Delete v4 todo: ' + id)
  return todos
}

async function markDone (id) {
  console.log('Mark done v4 todo: ' + id)
  const todo = (await axios.put('/api/v4/todos/' + id, { done: true })).data
  const index = findIndexById(todo.id)
  if (index !== undefined && index > -1) {
    todos[index].done = true
  }
  saveTodos()
}

async function markUnDone (id) {
  console.log('Mark UNdone v4 todo: ' + id)
  const todo = (await axios.put('/api/v4/todos/' + id, { done: false })).data
  const index = findIndexById(todo.id)
  if (index !== undefined && index > -1) {
    todos[index].done = false
  }
  saveTodos()
}

function clear () {
  todos.splice(0, todos.length)
  saveTodos()
  return todos
}

function findIndexById (id) {
  return todos.findIndex(todo => todo.id === id)
}

export default {
  add,
  get,
  remove,
  clear,
  markDone,
  markUnDone,
  refresh
}
