let todos = JSON.parse(localStorage.getItem('todos') || '[]')

function saveTodos () {
  localStorage.setItem('todos', JSON.stringify(todos))
}

function refresh () {
  todos = JSON.parse(localStorage.getItem('todos') || '[]')
  return new Promise((resolve) => {
    resolve(todos)
  })
}

function add (todo) {
  const newTodo = {
    id: Date.now(),
    done: false,
    text: todo
  }
  todos.push(newTodo)
  saveTodos()
  console.log(todos)
  return todos
}

function get () {
  return todos
}

function remove (id) {
  const index = findIndexById(id)
  if (index !== undefined && index > -1) {
    todos.splice(index, 1)
  }
  saveTodos()
  return todos
}

function markDone (id) {
  const index = findIndexById(id)
  if (index !== undefined && index > -1) {
    todos[index].done = true
  }
  saveTodos()
}

function markUnDone (id) {
  const index = findIndexById(id)
  if (index !== undefined && index > -1) {
    todos[index].done = false
  }
  saveTodos()
}

function clear () {
  todos.splice(0, todos.length)
  saveTodos()
  return todos
}

function findIndexById (id) {
  return todos.findIndex(todo => todo.id === id)
}

export default {
  add,
  get,
  remove,
  clear,
  markDone,
  markUnDone,
  refresh
}

export class todoLocalStorage {
}
