const linksData = [
  {
    title: 'Home',
    caption: 'starting page',
    icon: 'home',
    to: { path: '/' }
  },
  {
    title: 'Todo',
    caption: 'Todo app for reactive',
    icon: 'assignment_turned_in',
    to: { name: 'todo' }
  },
  {
    title: 'Docs',
    caption: 'quasar.dev',
    icon: 'school',
    link: 'https://quasar.dev'
  }
]

export function pageLinks () {
  return linksData
}
