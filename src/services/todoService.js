import { store } from 'src/store'
import todoWithLocalStorageService from './todoWithLocalStorageService'
import todoWithSimpleReactiveBackendService from './todoWithSimpleReactiveBackendService'
import todoWithMongoReactiveBackendService from 'src/services/todoWithMongoReactiveBackendService'

function getStorageService () {
  console.log('Get Storage service: ' + store.getters['todo/activeSource'])
  if (store.getters['todo/activeSource'] === 'backend-simple-reactive') {
    return todoWithSimpleReactiveBackendService
  }
  if (store.getters['todo/activeSource'] === 'backend-mongo-reactive') {
    return todoWithMongoReactiveBackendService
  }
  return todoWithLocalStorageService
}

async function reload () {
  console.log('Reloading todos')
  const todos = await getStorageService().refresh()
  console.log('Reloaded todos')
  console.log(todos)
  return todos
}

function add (todo) {
  console.log('Add todo: ' + todo)
  getStorageService().add(todo)
}

function get () {
  console.log('Get all todos')
  return getStorageService().get()
}

async function remove (id) {
  console.log('Remove todo: ' + id)
  return await getStorageService().remove(id)
}

async function markDone (id) {
  console.log('Mark todo done: ' + id)
  await getStorageService().markDone(id)
}

async function markUnDone (id) {
  console.log('Mark todo active: ' + id)
  await getStorageService().markUnDone(id)
}

function clear () {
  console.log('Clear todos')
  getStorageService().clear()
}

export default {
  add,
  get,
  remove,
  clear,
  markDone,
  markUnDone,
  reload
}
