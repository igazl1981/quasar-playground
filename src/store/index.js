import Vue from 'vue'
import Vuex from 'vuex'
import todo from './todo'

Vue.use(Vuex)

let store = null

export default function () {
  store = new Vuex.Store({
    modules: {
      todo
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  // if (process.env.DEV && module.hot) {
  //   module.hot.accept(['./todo'], () => {
  //     const newTodo = require('./todo').default
  //     store.hotUpdate({ modules: { todo: newTodo } })
  //   })
  // }

  return store
}

export { store }
