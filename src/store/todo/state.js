export default function () {
  return {
    showConfigDialog: false,
    config: {
      storageSources: ['local', 'backend-simple-reactive', 'backend-mongo-reactive'],
      activeSource: 'local'
    }
  }
}
