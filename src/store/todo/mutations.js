/*
export function someMutation (state) {
}
*/

export const setActiveSource = (state, source) => {
  state.config.activeSource = source
}

export const toggleConfigDialog = (state, enable) => {
  state.showConfigDialog = enable
}
