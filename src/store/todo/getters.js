/*
export function someGetter (state) {
}
*/

export const showConfigDialog = (state) => state.showConfigDialog

export const activeSource = (state) => state.config.activeSource

export const backendSources = (state) => state.config.storageSources
