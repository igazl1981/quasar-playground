/*
export function someAction (context) {
}
*/

export function hideConfigDialog ({ commit }) {
  commit('toggleConfigDialog', false)
}

export function showConfigDialog ({ commit }) {
  commit('toggleConfigDialog', true)
}
