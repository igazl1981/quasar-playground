# Trash Quasar App (quasar-trash)

A trash app for testing stuff

## Pages

The pages are different testing apps for training purposes.

### [TodoPage](./src/pages/TodoPage.vue)

This Todo is a very simple Todo app where multiple storage exist:

* localStorage
* backend-simple-reactive - `/v3/todos` endpoint
* backend-mongo-reactive - `/v4/todos` endpoint

The backend is on [gitlab](https://gitlab.com/igazl1981/todo-rest) where are  different endpoints to practice simple reactive programming.

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
